package com.devcamp.circlerestapi.circlerestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class Circle {

    private double radious = 1.0;

    public Circle() {
    }

    public Circle(double radious) {
        this.radious = radious;
    }

    public double getRadious() {
        return radious;
    }

    public void setRadious(double radious) {
        this.radious = radious;
    }

    public double getArea() {
        return Math.PI * radious * radious;

    }

    public double getCircumference() {
        return Math.PI * 2 * radious;
    }

    @Override
    public String toString() {
        return "Circle [radious=" + radious + "]";
    }

    @GetMapping("/circle")
    public ArrayList<Circle> CircleAreaList() {
        ArrayList<Circle> CircleList = new ArrayList<Circle>();

        Circle circle01 = new Circle(radious);
        Circle circle02 = new Circle(2.0);
        Circle circle03 = new Circle(3.0);
        System.out.println(circle01.getArea());
        System.out.println(circle01.getCircumference());
        System.out.println(circle01.toString());

        System.out.println(circle02.getArea());
        System.out.println(circle02.getCircumference());
        System.out.println(circle02.toString());

        System.out.println(circle03.getArea());
        System.out.println(circle03.getCircumference());
        System.out.println(circle03.toString());

        CircleList.add(0, circle01);
        CircleList.add(0, circle02);
        CircleList.add(0, circle03);

        return CircleList;
    }

}
